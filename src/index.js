import { renderBank } from "./components/bank.js"
import { fetchDevices, renderDeviceFeatures } from "./components/devices.js";
import { renderProduct } from "./components/product.js";
import { renderWork } from "./components/work.js";

const render = () => {
    renderBank();
    renderWork();
    renderDeviceFeatures();
    renderProduct();
}

const initializse = () => {
    fetchDevices();
    render();
}

initializse();