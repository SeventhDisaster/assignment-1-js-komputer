import { getBalance, resetLoanFlag, subtractFromBalance } from "./bank.js";
import { getSelectedDevice } from "./devices.js";

// Button
const purchaseBtn = document.getElementById('purchase-btn')

// Container
const productContainer = document.getElementById('product')

// Values
const productName = document.getElementById('product-name');
const productImage = document.getElementById('product-image');
const productDescription = document.getElementById('product-description');
const productPrice = document.getElementById('product-price');

purchaseBtn.addEventListener('click', () => {
    const { title, price } = getSelectedDevice();
    if(getBalance() < price) {
        alert("You don't have enough funds to purchase this product")
        return;
    }
    resetLoanFlag();
    subtractFromBalance(price)

    alert(`Congratulations! You're a proud new owner of the ${title}`)
})

productImage.addEventListener('error', () => {
    productImage.src = "./src/assets/image-notfound.png"
})

export const renderProduct = (device) => {
    if(!device) {
        productContainer.style.display = "none";
        return;
    }
    const {title, description, price, image} = device;
    productContainer.style.display = "flex";
    productName.innerText = title;
    productDescription.innerText = description;
    productPrice.innerText = `kr ${price},-`;

    productImage.src = `https://noroff-komputer-store-api.herokuapp.com/${image}`;
}