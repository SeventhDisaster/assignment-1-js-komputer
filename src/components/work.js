import { addToBalance, addToLoan, getLoan, paybackLoan, renderBank } from "./bank.js";

let pay = 0;

// Buttons
const workBtn = document.getElementById('work-btn')
const bankBtn = document.getElementById('bank-btn')
const payloanBtn = document.getElementById('payloan-btn')

// Values
const payElem = document.getElementById('pay-value');

workBtn.addEventListener('click', () => {
    pay += 100;
    renderWork();
});

bankBtn.addEventListener('click', () => {
    if(getLoan() > 0) {
        const deduction = pay * 0.1;
        pay -= deduction;
        addToLoan(-deduction);
        addToBalance(pay);
    } else {
        addToBalance(pay);
    }
    pay = 0;

    renderWork();
})

payloanBtn.addEventListener('click', () => {
    pay -= paybackLoan(pay);
    renderWork();
})

export const renderWork = () => {
    if(pay < 1) {
        bankBtn.disabled = true;
        bankBtn.classList.add("disabled");
        payloanBtn.disabled = true;
        payloanBtn.classList.add("disabled");
    } else {
        bankBtn.disabled = false;
        bankBtn.classList.remove("disabled");
        payloanBtn.disabled = false;
        payloanBtn.classList.remove("disabled");
    }

    payElem.innerText = `kr ${pay},-`;
    payloanBtn.style.display = getLoan() > 0 ? "block" : "none"
}

