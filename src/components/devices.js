import { renderProduct } from "./product.js";

let devices = [];
let selectedDevice;

const deviceList = document.getElementById('device-list');
const featureHead = document.getElementById('feature-head');
const deviceFeatures = document.getElementById('device-features'); 

export const fetchDevices = async () => {
    try {
        const url = `https://noroff-komputer-store-api.herokuapp.com/computers`
        const response = await fetch(url);
        const body = await response.json();
        devices = body;
    } catch (error) {
        console.error(error);
    } finally {
        renderDeviceList();
    }
}

deviceList.addEventListener('change', (e) => {
    if(e.target.value == '-') {
        selectedDevice = null;
    } else {
        selectedDevice = devices.find(device => device.id == e.target.value)
    }

    renderDeviceFeatures();
    renderProduct(selectedDevice);
})

export const getSelectedDevice = () => selectedDevice;

export const renderDeviceList = () => {
    devices.map(device => {
        const option = document.createElement("option");
        option.innerText = device.title;
        option.value = device.id;
        deviceList.appendChild(option);
    })
}

export const renderDeviceFeatures = () => {
    deviceFeatures.innerHTML = ""; // Clear features
    if(!selectedDevice) {
        featureHead.style.display = "none";
        deviceFeatures.style.display = "none";
        return;
    } else {
        const { specs } = selectedDevice;
        featureHead.style.display = "block";
        deviceFeatures.style.display = "block";
        specs.map(spec => {
            const featureElem = document.createElement("li");
            featureElem.innerText = spec;
            deviceFeatures.appendChild(featureElem);
        })
    }

}
