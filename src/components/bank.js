import { renderWork } from "./work.js";

let balance = 0;
let loan = 0;
let loanFlag = false;

export const resetLoanFlag = () => loanFlag = false;

// Button
const loanBtn = document.getElementById('loan-btn')

// Containers
const loanContainer = document.getElementById('loan');

// Values
const balanceElem = document.getElementById('balance-value');
const loanElem = document.getElementById('loan-value');

export const getBalance = () => balance;
export const addToBalance = (pay) => {
    balance += pay; 
    renderBank();
}  
export const subtractFromBalance = (price) => {
    balance -= price; 
    renderBank();
}
export const getLoan = () => loan;
export const addToLoan = (pay) => loan += pay;
export const paybackLoan = (pay) => {
    const remainder = loan - pay;
    if (remainder <= 0) {
        loan = 0;
        pay += remainder;
    } else {
        loan -= pay;
    }
    renderBank();
    return pay; // Returns remainder after paying back loan
}

loanBtn.addEventListener('click', () => {
    if(loan > 0) {
        alert('You must pay back your current loan before getting another loan.');
        return;
    }

    if(loanFlag) {
        alert('You can only get one loan before purchasing a computer');
        return;
    }

    const loanLimit = balance*2;
    let requestedLoan = parseInt(prompt(`How much would you like to loan? (Max: ${loanLimit})`))
    while(isNaN(requestedLoan)) {
        if(!requestedLoan) return;
        requestedLoan = parseInt(prompt(`Please provide a positive number value for your loan. (Max: ${loanLimit})`))
    }

    // The user has not loaned any money, so no action is taken. Also you can't loan negatives
    if(requestedLoan < 1) {
        return; 
    }

    if(requestedLoan > loanLimit) {
        alert("You cannot loan more than double your current bank balance.");
        return;
    }

    loan += requestedLoan;
    balance += requestedLoan;
    loanFlag = true;
    renderBank();
    renderWork();
})

export const renderBank = () => {
    if(loanFlag || loan) {
        loanBtn.classList.add("disabled");
    } else {
        loanBtn.classList.remove("disabled");
    }

    balanceElem.innerText = `kr ${balance},-`
    loanElem.innerText = `kr -${loan},-`;
    loanContainer.style.display = loan ? "flex" : "none"; // Display loan value if more than 0
}
