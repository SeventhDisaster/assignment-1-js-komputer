# Javascript Assignment 1 - Komputer Store
### Developers: Krister (SeventhDisaster) Emanuelsen
### Module: .NET Module 5 Javascript Fundamentals

## Deployment:
Deployed on [GitLab Pages](https://seventhdisaster.gitlab.io/assignment-1-js-komputer/)

Alternative URL: https://seventhdisaster.gitlab.io/assignment-1-js-komputer/

## Source:
https://gitlab.com/SeventhDisaster/assignment-1-js-komputer

___
## Description:

Here's your computer store app where you can buy computers and end up in crippling debt.
Everyone likes crippling debt right?

### Setup:
`Running from Live Server Extension in VSCode`
- Simply open the `index.html` file and enjoy the app on whatever port it goes to.

`Visiting the page`
- Visit `https://seventhdisaster.gitlab.io/assignment-1-js-komputer/`

## Design / Wireframe:

> Note: Actual design may differ slightly from the wireframe, as the wireframe was created prior to the app being developed. The real app includes the pay-back-loan button.

![Wireframe for this application](/wireframe.png "Application Wireframe")

# How to overuse flex: Starring me
I am not overusing flex.
It is a powerful and good tool, and there I am totally not addicted to using flex on literally everything.

One might question why I don't also use CSS Grid. But it is simply because my feeble brain cannot comprehend more than one dimension at a time. 2D planes are 2 complex 5 me.

Without flex I cannot even center a div.

Also I get to flex on everyone with my flex skills.